import './assets/main.css'
import DatePlugin from '@/plugins/date'


import { createApp } from 'vue'
import App from './App.vue'



createApp(App).use(DatePlugin).mount('#app')
