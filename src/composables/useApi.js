import { ref } from 'vue';
import {getMenus, formatData} from '../services/api.service';

export function useApi(){

    const menus = ref([]);
    const formattedMenus = ref([]);

    const fetchMenus = async () => {
        try{
            const response = await getMenus();

            menus.value = response.records; 

            formattedMenus.value = formatData(menus.value);

            console.log("format : " +formatData(menus.value));

        }catch(error){
            console.error('Erreur lors de la récupération des menus dans App.vue :', error);
        }
    }

    return{
        menus, 
        formattedMenus,
        fetchMenus 
    }
}