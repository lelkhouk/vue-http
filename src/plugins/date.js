
import dayjs from 'dayjs';

const DatePlugin = {
    install: (app, options) => {
        app.config.globalProperties.date = dayjs
    }
}

export default DatePlugin; 