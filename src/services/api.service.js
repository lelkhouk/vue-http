import instance from "@/config/axios";



export async function getMenus(){
    try{
        const response= await instance.get('dataset=menus_des_restaurants_scolaires');
        console.log(response);
        //const data = await response.json();
        return response.data;
    }catch(ex){
        console.log(ex)
    }
}

export function formatData (records){

    const formatMenus = []

    records.forEach((record)=>{
        formatMenus.push(
            {
            date: record.fields.plat_date,
	        plats: [{
                plat_libelle : record.fields.plat_libelle,
                plat_destination : record.fields.plat_destination ,
                plat_type : record.fields.plat_type
            }]
        })
    })

    return formatMenus
}


